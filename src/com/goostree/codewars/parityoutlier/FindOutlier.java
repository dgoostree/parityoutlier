package com.goostree.codewars.parityoutlier;

import java.util.Arrays;

public class FindOutlier {
	public static int find(int[] integers){
		
		int firstThree = 0;
		int targetMod = 0;
		int result = 0;
		
		//sum the abs of each of the first three values mod 2
		// 	Or, with Java 8:
		// 	firstThree = Arrays.stream(integers).limit(3).filter(i -> Math.abs(i % 2)).sum();
		for(int i = 0; i < 3; i++) {
			firstThree += Math.abs(integers[i] % 2);
		}
		
		//the outlier is odd if the sum calculated above is < 2, even otherwise
		if(firstThree < 2) {
			targetMod = 1;
		} else {
			targetMod = 0;
		}
		
		//find the match
		//	Or, with Java8:
		//	final Integer innerMod = new Integer(targetMod);
		//	result = Arrays.stream(integers)
		//					.filter( n -> Math.abs(n % 2) == innerMod)
		//					.findFirst()
		//					.getAsInt();
		
		for(int integer : integers) {
			if( Math.abs(integer % 2) == targetMod ){
				result = integer;
				break;
			}
		}
		
		return result;
	}
}
